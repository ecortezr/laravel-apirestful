@component('mail::message')
# Hola {{ $user->name }}.

Has cambiado tu correo electrónico. Por favor, verifícalo, usando el siguiente botón:

@component('mail::button', ['url' => route('verified', $user->verification_token) ])
Confirmar Cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }} Team
@endcomponent
