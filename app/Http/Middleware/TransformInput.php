<?php

namespace App\Http\Middleware;

use Illuminate\Validation\ValidationException;

use Closure;

class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $transformer)
    {
        /**
         * Bloque (hasta el return) para transformar los campos que llegan (POST/PUT/PATCH)
         */
        $transformedInput = [];

        // Almacena en el arreglo todos los campos que llegan en la petición, después de buscar su equivalente transformado, según el transformer que se recibe como parámetro
        foreach ($request->request->all() as $input => $value) {
            $transformedInput[$transformer::originalAttribute($input)] = $value;
        }

        // Reemplaza todos los inputs que llegaron, por los que están en el arreglo
        $request->replace($transformedInput);

        $response = $next($request);

        /**
         * Bloque para transformar los campos que se regresan en los métodos (POST/PUT/PATCH)
         */

        if (isset($response->exception) && $response->exception instanceof ValidationException) {
            // Si se va a devolver una exception y ésta, es de tipo ValidationException

            $data = $response->getData();

            $transformedErrors = [];

            foreach ($data->error as $field => $error) {
                $transformedField = $transformer::transformedAttribute($field);

                $transformedErrors[$transformedField] = str_replace($field, $transformedField, $error);
            }

            $data->error = $transformedErrors;

            $response->setData($data);

            return $response;
        }

        return $response;
    }
}
