<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Seller;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Transformers\ProductTransformer;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . ProductTransformer::class)->only(['store', 'update']);
        /**
         *  Es válido, usar las 2 invocaciones
         *
         *
        $this->middleware('scope:manage-products')->except('index');
        $this->middleware('scope:read-general')->only('index');*/

        /**
         * O, se puede dejar solo uno y hacer una validación, en el index
         */
        $this->middleware('scope:manage-products')->except('index');
        $this->middleware('can:view,seller')->only('index');
        $this->middleware('can:sale,seller')->only('store');
        $this->middleware('can:edit-product,seller')->only('update');
        $this->middleware('can:delete-product,seller')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        if (request()->user()->tokenCan('read-general') || request()->user()->tokenCan('manage-products')) {
            $products = $seller->products;

            return $this->showAll($products);
        }

        throw new AuthenticationException;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $seller)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'quantity' => 'required|integer|min:1',
            'image' => 'required|image'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['status'] = Product::PRODUCTO_NO_DISPONIBLE;
        // El primer argumento, es la ruta, la cual es la que establecimos por defecto en config/filesystems.php
        // El 2do argumento, no es necesario, dado que es el sistema de archivos por defecto que definimos en .env y luego especificamos en el archivo de configuración config/filesystems.php. Pudiese ser un espacio FTP, p.e.
        $data['image'] = $request->image->store('');
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);

        return $this->showOne($product, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller, Product $product)
    {
        $rules = [
            'quantity' => '|integer|min:1',
            'status' => 'in: '. Product::PRODUCTO_DISPONIBLE . ',' . Product::PRODUCTO_NO_DISPONIBLE,
            'image' => 'image'
        ];

        $this->validate($request, $rules);

        /*if ($seller->id != $product->seller_id) {
            // Si el vendedor no es el mismo que el que tiene asociado el producto, devolvemos un eror
            return $this->errorResponse('El vendedor especificado, no coincide con el vendedor asociado al producto', 422);
        }*/
        $this->verificarVendedor($seller, $product);

        $product->fill($request->only([
            'name',
            'description',
            'quantity'
        ]));

        if ($request->has('status')) {
            $product->status = $request->status;

            if ($product->estaDisponible() && ($product->categories()-count() == 0)) {
                return $this->errorResponse('Un producto activo, debe tener, al menos, una categoría', 409);
            }
        }

        if ($request->hasFile('image')) {
            Storage::delete($product->image);

            $product->image = $request->image->store('');
        }

        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar, al menos, un valor diferente para actualizar un producto', 422);
        }

        $product->save();

        return $this->showOne($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product)
    {
        $this->verificarVendedor($seller, $product);

        // Así se debe usar, para usar otro sistema de archivos, de los que estén configurados
        // Storage::disk('images')->delete($product->image);
        Storage::delete($product->image);

        $product->delete();

        return $this->showOne($product);
    }

    public function verificarVendedor(Seller $seller, Product $product)
    {
        if ($seller->id != $product->seller_id) {

            throw new HttpException(422, 'El vendedor especificado, no coincide con el vendedor asociado al producto');
            /* Si el vendedor no es el mismo que el que tiene asociado el producto, devolvemos un eror
            return $this->errorResponse('El vendedor especificado, no coincide con el vendedor asociado al producto', 422);*/
        }
    }
}
