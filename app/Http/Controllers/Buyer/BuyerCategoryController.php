<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerCategoryController extends ApiController
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,buyer')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        $categories = $buyer->transactions()->with('product.categories')
        ->get()
        ->pluck('product.categories') // Extrae la colección especificada (en este caso, las categorías) y la coloca en el primer nivel
        ->collapse() // Debido a la relación muchos a muchos, se evita la creacion de colleciones, dentro de colecciones. Este método, las integra, en una sola
        ->unique('id')
        ->values(); // Elimina los elementos vacios, dejados por los elementos duplicados, que elimina "unique"

        // dd($categories);

        return $this->showAll($categories);
    }
}
