<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerProductController extends ApiController
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,buyer')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        /**
         *
         * En las relaciones 1:N se devuelven Colecciones y no, instancias de los objetos relacionados. Por eso, no podemos hacer algo tan simple como $buyer-transactions->product;
        Es necesario, implementar Eager loading; en este caso, usando QueryBuilder.
        ****************************
        Se debe revisar la activación automática de Eager Loading (sin Usar Query Builder). Revisar videos de YouTube, "Sé un mejor desarrollador"
        *****************************
        */
        $products = $buyer->transactions()->with('product')->get()->pluck('product');

        // dd($products);

        return $this->showAll($products);
    }
}
